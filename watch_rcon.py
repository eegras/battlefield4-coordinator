import os
from battlefield_rcon.connection import RCONConnection
import time
from pprint import pprint
import random
import socket

# RCON COMMANDS:
# https://wiki.nitrado.net/en/Commands_in_Battlefield

DEBUG = False
HUNT_ON = True
RANDOMIZE = False
AUTOKICK = True

hostname = str(os.environ.get("RCON_HOST"))
password = str(os.environ.get("RCON_PASS"))
rconport = int(os.environ.get("RCON_PORT"))

player_whitelist = os.environ.get("PLAYER_WHITELIST", "Eegras").split(',')

hunt_map_list = list()
hunt_map_list.append(("XP5_Night_01", "Domination0", "2"))
#hunt_map_list.append(("XP6_CMP", "Domination0", "2"))
#hunt_map_list.append(("XP7_Valley", "Domination0", "2"))
#hunt_map_list.append(("XP4_WlkrFtry", "Domination0", "2"))
#hunt_map_list.append(("XP4_Titan", "Domination0", "2"))
#hunt_map_list.append(("XP2_001", "Domination0", "2"))
#hunt_map_list.append(("XP1_004", "Domination0", "2"))
#hunt_map_list.append(("XP1_003", "Domination0", "2"))
#hunt_map_list.append(("XP1_002", "Domination0", "2"))
#hunt_map_list.append(("XP1_001", "Domination0", "2"))
#hunt_map_list.append(("MP_Naval", "Domination0", "2"))

hunter_team = -1
prey_team = -1

player_list = dict()
player_list['hunters'] = list()
player_list['prey'] = list()

rcon_client = RCONConnection(remote_addr=hostname,
                             port=rconport,
                             password=password,
                             recv_buffer=8192)


def send_to_rcon(command):
    print("Command: ", " | ".join([str(f) for f in command]))
    r = rcon_client.send(command)
    print("Response:", r)
    return r


while True:
    try:
        rcon_client.connect()
        break
    except socket.timeout:
        print("Server not up...")
        time.sleep(5)


serverinfo = send_to_rcon(["serverinfo"])
print(f"Connected to : {serverinfo[1]}")

print("Reading server events, press CTRL+C for exit.")
players = dict()


def debug(msg):
    if DEBUG:
        send_to_rcon(["admin.say", msg, "all"])
        print(f"DEBUG: {msg}")


class Player(object):
    def __init__(self, playername, team, lives=1):
        self.playername = playername

        self.team = team
        self.lives = lives

    def knife_kill(self):
        self.lives += 1
        debug(f"{self.playername} knifed a prey!  Live added!")

    def died(self):
        self.lives -= 1
        debug(f"{self.playername} died!  Live removed!")

    def revived(self):
        self.lives += 1
        debug(f"{self.playername} was revived!  Live added!")

    def is_dead(self):
        return self.lives <= 0


def hunt_setup():
    say("Configuring hunt... Please wait.")
    send_to_rcon(['vars.idleBanRounds', '0'])
    send_to_rcon(['vars.idleTimeout', '86400'])

    say("Running next round.")

    #send_to_rcon(['mapList.runNextRound'])


def hunt_teardown():
    say("De-configuring hunt... Please wait.")
    send_to_rcon(['vars.idleBanRounds', '2'])
    send_to_rcon(['vars.idleTimeout', '0'])
    send_to_rcon(['vars.serverType', 'Ranked'])
    send_to_rcon(['vars.gamePassword'])


def say(msg, audience="all"):
    send_to_rcon(['admin.say', msg, audience])


def yell(msg, audience="all", duration=10):
    target_type, target, _ = f"{audience} ".lower().split(" ", 1)
    if target_type not in ("all", "player", "team"):
        print(f"{target_type} is not a valid audience!")
    if target_type == "team" and target not in ("1", "2"):
        print(f"{target} is not a valid team!")

    send_to_rcon(['admin.yell', msg, duration, target_type, target])


def update_player_list(reset=False, nomsg=False):
    global prey_team
    global hunter_team
    global players

    if reset:
        players = dict()

    player_info = send_to_rcon(["admin.listPlayers", "all"])[3:]
    offset = 10  # listPlayers is in sets of 11 values
    team_one = 0
    team_two = 0
    for i in range(1, (len(player_info) // offset)):
        player = player_info[i * offset:i*offset+offset]
        existing_player = players.get(player[0], None)
        existing_lives = existing_player.lives if existing_player else 1
        players[player[0]] = Player(playername=player[0], team=player[2], lives=existing_lives)
        if player[2] == 1:
            team_one += 1
        if player[2] == 2:
            team_two += 1

    if team_one < team_two:
        prey_team, hunter_team = 1, 2
    else:
        prey_team, hunter_team = 2, 1

    if DEBUG:
        debug(f"Detected {len(players)} players.")
        for p in players:
            player = players[p]
            debug(f"{player.playername} team {player.team} lives {player.lives}")
        debug(f"Prey team {prey_team}")

    print("Team Notice")
    if HUNT_ON and not nomsg:
        send_to_rcon(["admin.yell", "You are on Team Hunter", "10", "team", hunter_team])
        send_to_rcon(["admin.yell", "You are on Team Prey", "10", "team", prey_team])


def randomize_players():
    print("Randomzing players")
    all_players = list(players.keys())
    team_choices = ['1', '2']
    random.shuffle(all_players)
    random.shuffle(team_choices)

    # Top half team one, bottom half team two.
    half = len(all_players) // 2
    for i in range(0, len(all_players)):
        print(f"{i} {half}")
        if i < half:
            print(f"Moving {all_players[i]} to team {team_choices[0]}")
            send_to_rcon(["admin.movePlayer", all_players[i], team_choices[0], "0", "true"])
            print(f"admin.movePlayer {all_players[i]} {team_choices[0]} 0 true")
        else:
            print(f"Moving {all_players[i]} to team {team_choices[1]}")
            print(f"admin.movePlayer {all_players[i]} {team_choices[1]} 0 true")
            send_to_rcon([f"admin.movePlayer", all_players[i], team_choices[1], "0", "true"])

def go_next_round():
    time.sleep(5)
    send_to_rcon(["mapList.runNextRound"])


def enforce_private(event):
    if event[1] not in player_whitelist:
        print(f"Kicking {event[1]}...")
        r = send_to_rcon(["admin.kickPlayer", event[1]])
    else:
        # Update player list
        update_player_list()


def enforce_lives(event):
    spawner = players[event[1]]
    if spawner.team == hunter_team and spawner.lives <= 0:
        say(f"You are out of lives.  You cannot respawn.", audience=f"player {spawner.playername}")
        send_to_rcon(['admin.killPlayer', spawner.playername])


def handle_kill(event):
    killer = players.get(event[1])
    victim = players.get(event[2])
    method = event[3]

    if victim.team == prey_team and killer.team == hunter_team and method == "Melee":
        killer.knife_kill()

    if victim.team == hunter_team:
        players[event[2]].died()
        if victim.is_dead():
            debug(f"{victim.playername} is out of lives! ")
            yell("You are out of lives!", duration=20,
                 audience=f"player {victim.playername}")
            check_win()
        else:
            debug(f"{victim.playername} has {victim.lives} live(s) left! ")
            send_to_rcon(["admin.yell",
                              "You have {victim.lives} live(s) left!",
                              "20",
                              f"player {victim.playername}"])


def check_win():
    alive = list()
    for p in players:
        player = players[p]
        if player.team == hunter_team and not player.is_dead():
            alive.append(f"{player.playername}: {player.lives}")
    if not alive:
        send_to_rcon(["admin.yell", "Game is over!  Prey wins!", "20", "all"])
        time.sleep(5)
        send_to_rcon(["mapList.endRound", prey_team])
    else:
        send_to_rcon(["admin.say", f"Still alive: {', '.join(alive)}", "all"])


if HUNT_ON:
    hunt_setup()


update_player_list()
try:
    for heard_event in rcon_client.read_events():
        print(heard_event)
        if AUTOKICK and heard_event[0] == "player.onAuthenticated":
            enforce_private(heard_event)

        if HUNT_ON:
            if heard_event[0] == "player.onKill":
                handle_kill(heard_event)
            if heard_event[0] == "player.onSpawn":
                update_player_list(nomsg=True)
                enforce_lives(heard_event)
            if heard_event[0] == "player.onTeamChange":
                update_player_list()
            if heard_event[0] == "player.onDisconnect":
                players.pop(heard_event[1], None)
                update_player_list(nomsg=True)
            if heard_event[0] == "server.onLevelLoaded":
                update_player_list(reset=True)
                if RANDOMIZE:
                    randomize_players()

        if heard_event[0] == "server.onRoundOver":
            go_next_round()
        if heard_event[0] == "player.onChat" and heard_event[2] == "!ping":
            send_to_rcon(["admin.say", "pong", f"20 player {heard_event[1]}"])
        if heard_event[0] == "player.onChat" and heard_event[2] == "!hunt_load" and heard_event[1] in player_whitelist:
            update_player_list(reset=True)
        if heard_event[0] == "player.onChat" and heard_event[2] == "!hunt_on" and heard_event[1] in player_whitelist:
            hunt_setup()
            send_to_rcon(["admin.say", "Hunt Enabled!", f"20 player {heard_event[1]}"])
            HUNT_ON = True
        if heard_event[0] == "player.onChat" and heard_event[2] == "!hunt_off" and heard_event[1] in player_whitelist:
            hunt_teardown()
            yell("Hunt Disabled!", audience=f"player {heard_event[1]}"])
            HUNT_ON = False
        if heard_event[0] == "player.onChat" and heard_event[2] == "!debug_on" and heard_event[1] in player_whitelist:
            send_to_rcon(["admin.say", "Debug Enabled!", f"20 player {heard_event[1]}"])
            DEBUG = True
        if heard_event[0] == "player.onChat" and heard_event[2] == "!debug_off" and heard_event[1] in player_whitelist:
            send_to_rcon(["admin.say", "Debug Disabled!", f"20 player {heard_event[1]}"])
            DEBUG = False
        if heard_event[0] == "player.onChat" and heard_event[2] == "!randomize" and heard_event[1] in player_whitelist:
            update_player_list()
            randomize_players()
except KeyboardInterrupt:
    rcon_client.disconnect()
except Exception as e:
    print(e)
